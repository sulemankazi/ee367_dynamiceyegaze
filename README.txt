REQUIREMENTS:

1. Download this repository from bitbucket. We are assuming the code is in the CODE folder (so the CODE folder has subfolders EyeDepth/) .

2. Install Tobii EyeX eye tracker software:
   http://developer.tobii.com/eyex-setup/

3. Download Tobii EyeX SDK and unzip into the code folder, so  the CODE folder has a new folder that looks like this:
	CODE/TobiiEyeXSdk-Cpp-1.6.477/

4. Install QT Creator (MSVC2013 64bit):
	http://www.qt.io/download-open-source/

5. Install MSVC2013 (You can get this by install MS Visual Studio 2013 Express which is free):
	https://www.microsoft.com/en-us/download/details.aspx?id=48131

6. Open the EyeDepth.pro file in the CODE/EyeDepth/ folder with QT creator and build it

7. Connect the Tobii Eyetracker and perform callibration.

8. Run the program from within QT Creator, you might need to adjust the size of the UI if your screen resolution is too small or too large. If there is no image showing up in the UI make sure your build folder has the images/ and images2/ subfolders in it.


	
 