#ifndef EYETRACKER_H
#define EYETRACKER_H

#include <QMainWindow>
#include <eyex/EyeX.h>
#include<Windows.h>
#include<assert.h>
#include <stdio.h>
#include <conio.h>
#include<iostream>

class eyeTracker
{
public:
    eyeTracker();
    void getEyePosition(float& X, float& Y);
    ~eyeTracker();
};


#endif // EYETRACKER_H
