#-------------------------------------------------
#
# Project created by QtCreator 2016-02-25T14:27:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EyeDepth
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    eyetracker.cpp \
    worker.cpp

HEADERS  += mainwindow.h \
    eyetracker.h \
    worker.h

FORMS    += mainwindow.ui

# CONFIG += c++11

INCLUDEPATH +=$$PWD/../TobiiEyeXSdk-Cpp-1.6.477/include/
QMAKE_CXXFLAGS_RELEASE -= -Zc:strictStrings

LIBS += -L$$PWD/../TobiiEyeXSdk-Cpp-1.6.477/lib/x64/ -lTobii.EyeX.Client

INCLUDEPATH += $$PWD/../TobiiEyeXSdk-Cpp-1.6.477/lib/x64
DEPENDPATH += $$PWD/../TobiiEyeXSdk-Cpp-1.6.477/lib/x64
