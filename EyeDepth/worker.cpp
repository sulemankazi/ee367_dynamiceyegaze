#include <QApplication>
#include "worker.h"
#include <Windows.h>

Worker::Worker()
    : m_running(true)
{
}

void Worker::doWork()
{
    float X;
    float Y;

    while (m_running)
    {
        tracker.getEyePosition(X,Y);

        point.setX(X);
        point.setY(Y);

        emit updatePosition(point);
        Sleep(20);
    }

    emit finished();
}

void Worker::stopWork()
{
    m_running = false;
}
