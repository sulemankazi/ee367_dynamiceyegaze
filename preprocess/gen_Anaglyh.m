path2files = 'D:\Winter_15\EE367\project\';
L_files = dir([path2files 'L_*.png']);
R_files = dir([path2files 'R_*.png']);
length(L_files)
length(R_files)
for id = 1:length(L_files)
    Im_L{id} = im2double(imread([path2files L_files(id).name]));
    Im_R{id} = im2double(imread([path2files R_files(id).name]));
    Stereo_An{id} = stereoAnaglyph(Im_L{id},Im_R{id});
    imwrite(Stereo_An{id},['Stereo_An_depth' num2str(id) '.jpg']);
end

%% All in focus stereo

L_all_file = im2double(imread([path2files 'L_All_in_focus.jpg']));
R_all_file = im2double(imread([path2files 'R_All_in_focus.jpg']));
Stereo_all = stereoAnaglyph(L_all_file,R_all_file);
imwrite(Stereo_all,'Stereo_All_Focus_An_depth.jpg');

