/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *xPos;
    QLabel *yPos;
    QLabel *label;
    QLabel *textLabel02;
    QGraphicsView *graphicsView;
    QLabel *connectionLabel;
    QPushButton *connectionButton;
    QPushButton *changeImageButton;
    QMenuBar *menuBar;
    QMenu *menuEyeGaze_Viewer;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1500, 1000);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setAutoFillBackground(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        xPos = new QLabel(centralWidget);
        xPos->setObjectName(QStringLiteral("xPos"));
        xPos->setGeometry(QRect(1690, 910, 55, 16));
        yPos = new QLabel(centralWidget);
        yPos->setObjectName(QStringLiteral("yPos"));
        yPos->setGeometry(QRect(1690, 930, 55, 16));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(1660, 910, 21, 16));
        textLabel02 = new QLabel(centralWidget);
        textLabel02->setObjectName(QStringLiteral("textLabel02"));
        textLabel02->setGeometry(QRect(1660, 930, 21, 16));
        graphicsView = new QGraphicsView(centralWidget);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));
        graphicsView->setGeometry(QRect(20, 9, 1600, 1050));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(graphicsView->sizePolicy().hasHeightForWidth());
        graphicsView->setSizePolicy(sizePolicy1);
        graphicsView->setMaximumSize(QSize(1920, 1080));
        graphicsView->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        connectionLabel = new QLabel(centralWidget);
        connectionLabel->setObjectName(QStringLiteral("connectionLabel"));
        connectionLabel->setGeometry(QRect(1650, 890, 91, 16));
        connectionButton = new QPushButton(centralWidget);
        connectionButton->setObjectName(QStringLiteral("connectionButton"));
        connectionButton->setGeometry(QRect(1640, 950, 121, 61));
        changeImageButton = new QPushButton(centralWidget);
        changeImageButton->setObjectName(QStringLiteral("changeImageButton"));
        changeImageButton->setGeometry(QRect(1780, 950, 121, 61));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1500, 26));
        menuEyeGaze_Viewer = new QMenu(menuBar);
        menuEyeGaze_Viewer->setObjectName(QStringLiteral("menuEyeGaze_Viewer"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuEyeGaze_Viewer->menuAction());

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "eyeGaze", 0));
        xPos->setText(QApplication::translate("MainWindow", "None", 0));
        yPos->setText(QApplication::translate("MainWindow", "None", 0));
        label->setText(QApplication::translate("MainWindow", "X: ", 0));
        textLabel02->setText(QApplication::translate("MainWindow", "Y: ", 0));
        connectionLabel->setText(QApplication::translate("MainWindow", "Disconnected", 0));
        connectionButton->setText(QApplication::translate("MainWindow", "Connect", 0));
        changeImageButton->setText(QApplication::translate("MainWindow", "Next Image", 0));
        menuEyeGaze_Viewer->setTitle(QApplication::translate("MainWindow", "eyeGaze Viewer", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
