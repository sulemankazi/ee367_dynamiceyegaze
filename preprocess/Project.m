clc; clear ;
path2data = 'StereoLightFields\';
scenes = dir([path2data 'scene*']);
% choose which scene to run
for file_id = 1
    Im_path = dir([path2data scenes(file_id).name '\*.png']);
    for j = 1:length(Im_path)
        Im_lytro{file_id}{j} = im2double(imread([path2data scenes(file_id).name '\' Im_path(j).name]));
    end
end
%% TEST TO SEE
% imshow(Im_lytro{1}{1}(7:14:end,7:14:end,:).^(1/2.2)); %first is right
% figure
% imshow(Im_lytro{1}{2}(7:14:end,7:14:end,:).^(1/2.2)); % second is left
% Im_R = Im_lytro{1}{1}(7:14:end,7:14:end,:).^(1/2.2);
% imwrite(Im_R,'Project_Exp_scene1_Right.jpg');
% Im_L = Im_lytro{1}{2}(7:14:end,7:14:end,:).^(1/2.2);
% Stereo_An = stereoAnaglyph(Im_L,Im_R);
% imshow(Stereo_An)
% find the correspondences and do ransac to get F matrix and then rectify

%% Lytro Refocus Code
scene =1;
[row,col,ch]=size(Im_lytro{scene}{1});
mLens = 14;
sub_row = row/mLens;
sub_col = col/mLens;
%LightField = zeros(sub_row,sub_col,mLens,mLens,ch);
Im_cat = zeros(row,col,ch);
for str = 1:2 % both left and right
    for ky = 1:mLens
        for kx = 1:mLens
            LightField{str}(:,:,ky,kx,:) = Im_lytro{scene}{str}(ky:mLens:end,kx:mLens:end,:);
            Im_cat((ky-1)*sub_row+1:ky*sub_row,(kx-1)*sub_col+1:kx*sub_col,:) = ...
                reshape(LightField{str}(:,:,ky,kx,:),[sub_row,sub_col,ch]);
        end
    end
end
%% No shift Image
for str = 1:2
    Summed_Im = zeros(sub_row,sub_col,ch);
    for ky=1:mLens
        for kx=1:mLens
            Summed_Im = Summed_Im + reshape(LightField{str}(:,:,ky,kx,:),[sub_row,sub_col,ch]);
        end
    end
    Avg_Im = Summed_Im / mLens^2;
    figure,imshow(Avg_Im.^(1/2.2))
end
%% Shift Images to change depth focus
maxU=floor((mLens-1)/2);
maxV=maxU;
Shift = -3:2;
[X,Y]=meshgrid(1:sub_col,1:sub_row);
file_name{1}='R_';
file_name{2}='L_';
for str = 1:2
    for id =1:length(Shift)
        Shifted_Im = zeros(sub_row,sub_col,ch);
        for ky=1:mLens
            for kx=1:mLens
                u=ky-1-maxU;
                v=kx-1-maxV;
                Shft_x = (v/maxV)*Shift(id);
                Shft_y = (u/maxU)*Shift(id);
                Inp_Im = reshape(LightField{str}(:,:,ky,kx,:),[sub_row,sub_col,ch]);
                temp_Im(:,:,1)  = interp2(X,Y,Inp_Im(:,:,1),X+Shft_x,Y+Shft_y,'cubic',0);
                temp_Im(:,:,2)  = interp2(X,Y,Inp_Im(:,:,2),X+Shft_x,Y+Shft_y,'cubic',0);
                temp_Im(:,:,3)  = interp2(X,Y,Inp_Im(:,:,3),X+Shft_x,Y+Shft_y,'cubic',0);
                Shifted_Im = Shifted_Im + temp_Im;
            end
        end
        Avg_Shift_Im = Shifted_Im / mLens^2;
        imwrite(Avg_Shift_Im.^(1/2.2),[file_name{str} num2str(id) 'shift by' num2str(Shift(id)) '.png']);
        %title(['shift by' num2str(id)])
        %pause(0.5)
    end
end
%% ALL IN FOCUS SECTION
L_files = dir(['L_*.png']);
R_files = dir(['R_*.png']);
Im = cell(length(L_files),2);
for id = 1:length(L_files)
    Im{id,1}=im2double(imread([ R_files(id).name]));
    Im{id,2}=im2double(imread([ L_files(id).name]));
end

% Gradients for each Image
[row, col, ch] = size(Im{1,1});
for id = 1:length(L_files)
    for str = 1:2 % 1-right and 2-left
        for ch=1:3
            Im_Dy{id,str}{ch} = diff(Im{id,str}(:,:,ch));
            Im_Dy{id,str}{ch} = Im_Dy{id,str}{ch}(:,1:end-1);
            Im_Dx{id,str}{ch} = diff(Im{id,str}(:,:,ch),1,2);
            Im_Dx{id,str}{ch} = Im_Dx{id,str}{ch}(1:end-1,:); % cropping to have same size of both gradients
        end
    end
end

%Grad_Sum = zeros([size(Im_Dy{1,1}{1}),length(L_files)]);
for id=1:length(L_files)
    for str = 1:2
        Grad_Sum{str}(:,:,id) = sqrt(Im_Dy{id,str}{1}.^2 + Im_Dx{id,str}{1}.^2) + ...
                   sqrt(Im_Dy{id,str}{2}.^2 + Im_Dx{id,str}{2}.^2) + ...
                   sqrt(Im_Dy{id,str}{3}.^2 + Im_Dx{id,str}{3}.^2) ;
    end
end

for str=1:2
    [Val{str},IND{str}] = max(Grad_Sum{str},[],3);
    Fin_Im{str} = zeros(row,col,3);
end


for str=1:2
    for id = 1:row-1
        for j=1:col-1
            Fin_Im{str}(id,j,:)=Im{IND{str}(id,j),str}(id,j,:);
        end
    end
    imwrite(Fin_Im{str},[file_name{str} 'All_in_focus.jpg']);
end

% imagesc(IND);
% axis tight equal; colorbar;

%% HSV
shift_focus=-5:0;
for str=1:2
    H=shift_focus(IND{str}); S=ones(size(IND{str}));V=Val{str};
    % rescale
    H=H-min(H(:));H=H./max(H(:));
    V=V-min(V(:));V=V./max(V(:));
    Im_HSV = cat(3,H,S,V);
    imwrite(hsv2rgb(Im_HSV),[file_name{str} 'Depth_Map_Rough.jpg']);
    %axis tight equal off
end