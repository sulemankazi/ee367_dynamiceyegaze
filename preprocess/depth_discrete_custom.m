% discretise depth map
clc
clear
path2file = 'child\';
Im= im2double(imread([path2file 'depth.png']));
figure, imshow(Im)
Im = imresize(Im, [800 1200]);
%figure, imshow(Im)
% smoothen the depth map
H = fspecial('gaussian',[21 21],5);
H = fspecial('disk',25);
Im = imfilter(Im,H,'replicate');
%figure, imshow(Im)
Im = Im - min(Im(:));
Im = Im.*(1./max(Im(:)));
%figure, imshow(Im)
NumLevel = 3
Levels = NumLevel - 0.01;
Im = floor(Im.*(Levels));
figure,imshow(uint8(Im.*255/(NumLevel-1)))
imwrite(uint8(Im.*127),[path2file 'Im_child_depth.png']);