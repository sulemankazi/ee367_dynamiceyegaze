#include "mainwindow.h"
#include "ui_mainwindow.h"



// Main Window

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    
    setupImages();
    dataStreaming = false;
    currentImage = 0;
    WIDTH =  1500;
    HEIGHT = 1000;

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::stopDataStream()
{
    dataStreaming = false;
}


void MainWindow::updatePosition(QPointF newPosition)
{    
    static int imageX = newPosition.x();
    static int imageY = newPosition.y();
    static int alpha =0.9;

    ui->xPos->setText(QString::number(imageX));
    ui->yPos->setText(QString::number(imageY));

    if(newPosition.x() > 0 && newPosition.x() < WIDTH && newPosition.y()> 0 && newPosition.y() < HEIGHT)
    {
        imageX = imageX*alpha+(1-alpha)*newPosition.x();
        imageY = imageY*alpha+(1-alpha)*newPosition.y();
    }
    if(imageX > 0 && imageX < WIDTH && imageY > 0 && imageY <HEIGHT)
    {

         QColor currentColor( depthMapImage.pixel( imageX, imageY ) );         

         if(currentImage ==0)
            changeImage1(currentColor.red());

         else if(currentImage ==1)
            changeImage2(currentColor.red());
    }
}


void MainWindow::setupImages()
{

    setupImageBatch_1();
    setupImageBatch_2();
}


void MainWindow::setupImageBatch_1()
{
    QPixmap image;
    QImage  *imageObject;
    imageObject = new QImage();

    depthMapImage.load("images/depthMap.png");
    depthMap = QPixmap::fromImage(depthMapImage);


    imageObject->load("images/AllInFocus.jpg");

    image = QPixmap::fromImage(*imageObject);

    //Default


    defaultScene = new QGraphicsScene(this);
    defaultScene->addPixmap(image);
    defaultScene->setSceneRect(image.rect());

    imageObject->load("images/0.jpg");
    image = QPixmap::fromImage(*imageObject);

    // 0
    scene1 = new QGraphicsScene(this);
    scene1->addPixmap(image);
    scene1->setSceneRect(image.rect());

    imageObject->load("images/127.jpg");
    image = QPixmap::fromImage(*imageObject);

    // 127
    scene2 = new QGraphicsScene(this);
    scene2->addPixmap(image);
    scene2->setSceneRect(image.rect());

    imageObject->load("images/254.jpg");
    image = QPixmap::fromImage(*imageObject);

    // 254
    scene3 = new QGraphicsScene(this);
    scene3->addPixmap(image);
    scene3->setSceneRect(image.rect());

    ui->graphicsView->setScene(defaultScene);
}

void MainWindow::setupImageBatch_2()
{
    QPixmap image;
    QImage  *imageObject;
    imageObject = new QImage();

    depthMapImage2.load("images/AllInFocus.jpg");
    depthMap2 = QPixmap::fromImage(depthMapImage2);


    imageObject->load("images/AllInFocus.jpg");
    image = QPixmap::fromImage(*imageObject);

    //Default
    defaultScene2 = new QGraphicsScene(this);
    defaultScene2->addPixmap(image);
    defaultScene2->setSceneRect(image.rect());

    imageObject->load("images/AllInFocus.jpg");
    image = QPixmap::fromImage(*imageObject);

    // 0
    scene1_2 = new QGraphicsScene(this);
    scene1_2->addPixmap(image);
    scene1_2->setSceneRect(image.rect());

    imageObject->load("images/AllInFocus.jpg");
    image = QPixmap::fromImage(*imageObject);

    // 127
    scene2_2 = new QGraphicsScene(this);
    scene2_2->addPixmap(image);
    scene2_2->setSceneRect(image.rect());

    imageObject->load("images/AllInFocus.jpg");
    image = QPixmap::fromImage(*imageObject);

    // 254
    scene3_2 = new QGraphicsScene(this);
    scene3_2->addPixmap(image);
    scene3_2->setSceneRect(image.rect());

}

void MainWindow::changeImage1(int imageIndex)
{

    if(imageIndex == 0)
        ui->graphicsView->setScene(scene1);


    if(imageIndex == 127)
        ui->graphicsView->setScene(scene2);


    if(imageIndex == 254)
        ui->graphicsView->setScene(scene3);
}

void MainWindow::changeImage2(int imageIndex)
{

    if(imageIndex == 0)
        ui->graphicsView->setScene(scene1_2);


    if(imageIndex == 127)
        ui->graphicsView->setScene(scene2_2);


    if(imageIndex == 254)
        ui->graphicsView->setScene(scene3_2);
}

void MainWindow::on_connectionButton_clicked()
{
    if(!dataStreaming)
    {

        QThread *workerThread;
        Worker *worker;

        workerThread = new QThread;
        worker = new Worker;
        worker->moveToThread(workerThread);

        connect(workerThread, SIGNAL(started()), worker, SLOT(doWork()));
        connect(worker, SIGNAL(finished()), workerThread, SLOT(quit()));
        connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
        connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
        connect(worker, SIGNAL(finished()), this, SLOT(stopDataStream()));

        connect(worker, SIGNAL(updatePosition(QPointF)), this, SLOT(updatePosition(QPointF)));
        workerThread->start();

        dataStreaming = true;
        ui->connectionLabel->setText("Connected");

    }

}

void MainWindow::on_changeImageButton_clicked()
{
    currentImage++;
    currentImage %= 2;

    if(currentImage ==0)
    {
        WIDTH =  1500;
        HEIGHT = 1000;
    }
    else if (currentImage ==1)
    {
        WIDTH =  1000;
        HEIGHT = 1000;
    }

    if(!dataStreaming)
    {
        if(currentImage ==0)
        {
             ui->graphicsView->setScene(defaultScene);
        }
        else if (currentImage ==1)
        {
            ui->graphicsView->setScene(defaultScene2);
        }

    }
}
