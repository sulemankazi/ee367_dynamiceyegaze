#ifndef __WORKER_H__
#define __WORKER_H__

#include <QObject>
#include <eyetracker.h>
#include<QPointF>

class Worker : public QObject
{
    Q_OBJECT

    public:
        Worker();

    public slots:
        void doWork();
        void stopWork();

    signals:
        void updatePosition(QPointF);
        void finished();

    private:
        bool m_running;
        eyeTracker tracker;
        QPointF point;
};

#endif // Worker
