Note: There is only one data file presently as the data files are too big.
1. Run the Project.m file. 
   a. It converts stereo light fields into a focal stack for both Left and Right .
   b. Section 'Shift Images to change depth focus' creates the focal stack 
      wherein the Shift paramter decides the shift for each subimage and decides
      the depth at which image will be focused.
   c. The 'ALL IN FOCUS SECTION' creates an all in focus image for each of the left and right stacks.
   d. The 'HSV' section creates a depth map using the all in focus image.

2. The gen_Anaglyph.m function creates an anaglyph given two stereo images.
   Note: This function didn't work on the stereo light fields as the baseline disparity was huge.
         Thus we used the single-viewpoint images and created a novel view in Photoshop by manually 
         shifting the focal plane.
 
  

    
      