#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <eyetracker.h>
#include <worker.h>
#include <QThread>
#include<QPointF>
#include<QGraphicsScene>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);    
    ~MainWindow();  

private slots:    
    void updatePosition(QPointF newPosition);
    void stopDataStream();
    void on_connectionButton_clicked();
    void on_changeImageButton_clicked();

private:
    Ui::MainWindow *ui;    
    bool dataStreaming;
    void setupImages();
    void setupImageBatch_1();
    void setupImageBatch_2();
    void changeImage1(int imageIndex);
    void changeImage2(int imageIndex);
    int currentImage;
    int WIDTH;
    int HEIGHT;


    QPixmap depthMap;
    QImage depthMapImage;
    QGraphicsScene *defaultScene;
    QGraphicsScene *scene1;
    QGraphicsScene *scene2;
    QGraphicsScene *scene3;

    QPixmap depthMap2;
    QImage depthMapImage2;
    QGraphicsScene *defaultScene2;
    QGraphicsScene *scene1_2;
    QGraphicsScene *scene2_2;
    QGraphicsScene *scene3_2;
};

#endif // MAINWINDOW_H
